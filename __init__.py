# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import abreviated_journal
from . import account_es
from . import common
from . import general_ledger
from . import journal
from . import taxes_by_invoice
from . import trial_balance


def register():
    Pool.register(
        common.Account,
        common.Party,
        journal.PrintJournalStart,
        abreviated_journal.PrintAbreviatedJournalStart,
        general_ledger.PrintGeneralLedgerStart,
        trial_balance.PrintTrialBalanceStart,
        taxes_by_invoice.PrintTaxesByInvoiceAndPeriodStart,
        taxes_by_invoice.Invoice,
        common.FiscalYear,
        module='account_jasper_reports', type_='model')
    Pool.register(
        journal.PrintJournal,
        abreviated_journal.PrintAbreviatedJournal,
        general_ledger.PrintGeneralLedger,
        trial_balance.PrintTrialBalance,
        taxes_by_invoice.PrintTaxesByInvoiceAndPeriod,
        module='account_jasper_reports', type_='wizard')
    Pool.register(
        journal.JournalReport,
        abreviated_journal.AbreviatedJournalReport,
        general_ledger.GeneralLedgerReport,
        trial_balance.TrialBalanceReport,
        taxes_by_invoice.TaxesByInvoiceReport,
        taxes_by_invoice.TaxesByInvoiceAndPeriodReport,
        module='account_jasper_reports', type_='report')
    Pool.register(
        account_es.TaxCodeTemplate,
        account_es.TaxCode,
        account_es.PrintTaxesByInvoiceAndPeriodStart,
        module='account_jasper_reports', type_='model',
        depends=['account_es'])
    Pool.register(
        account_es.PrintTaxesByInvoiceAndPeriod,
        module='account_jasper_reports', type_='wizard',
        depends=['account_es'])
    Pool.register(
        account_es.TaxesByInvoiceReport,
        module='account_jasper_reports', type_='report',
        depends=['account_es'])
    Pool.register(
        taxes_by_invoice.InvoiceAeatSii,
        module='account_jasper_reports', type_='model',
        depends=['aeat_sii'])
    Pool.register(
        taxes_by_invoice.TaxesByInvoiceReportAeatSii,
        module='account_jasper_reports', type_='report',
        depends=['aeat_sii'])
    Pool.register(
        taxes_by_invoice.InvoiceAccountSii,
        module='account_jasper_reports', type_='model',
        depends=['account_es_sii'])
    Pool.register(
        taxes_by_invoice.TaxesByInvoiceReportAccountSii,
        module='account_jasper_reports', type_='report',
        depends=['account_es_sii'])
