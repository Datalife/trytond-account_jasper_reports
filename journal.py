# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import csv
from datetime import timedelta
from decimal import Decimal
from io import BytesIO, TextIOWrapper
from itertools import groupby
from sql import Null
from sql.conditionals import Case, Coalesce
from sql.functions import Extract
from sql.operators import Concat
from trytond.model import ModelView, fields
from trytond.modules.jasper_reports.jasper import JasperReport
from trytond.pool import Pool
from trytond.pyson import Bool, Eval, If
from trytond.tools import reduce_ids
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateView, Wizard


class PrintJournalStart(ModelView):
    'Print Journal'
    __name__ = 'account_jasper_reports.print_journal.start'
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)
    start_period = fields.Many2One('account.period', 'Start Period',
        required=True,
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            If(Bool(Eval('end_period')),
                ('start_date', '<=', (Eval('end_period'), 'start_date')),
                (),
                )
            ])
    end_period = fields.Many2One('account.period', 'End Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            If(Bool(Eval('start_period')),
                ('start_date', '>=', (Eval('start_period'), 'start_date')),
                (),
                )
            ])
    open_close_account_moves = fields.Boolean('Create Open/Close Moves',
        help="If this field is checked and Start Period is 01 and the fiscal "
        "year before are closed, an open move of this year will be created. "
        "And if 12 is in End Period and the fiscal year are closed a "
        "close move of the year are created. And will use all journals.")
    open_move_description = fields.Char('Open Move Description',
        states={
            'invisible': ~Bool(Eval('open_close_account_moves')),
            })
    close_move_description = fields.Char('Close Move Description',
        states={
            'invisible': ~Bool(Eval('open_close_account_moves')),
            })
    journals = fields.Many2Many('account.journal', None, None, 'Journals',
        states={
            'readonly': Bool(Eval('open_close_account_moves')),
            })
    output_format = fields.Selection([
            ('pdf', 'PDF'),
            ('xls', 'XLS'),
            ('csv', 'CSV'),
            ], 'Output Format', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_output_format():
        return 'pdf'

    @fields.depends('fiscalyear')
    def on_change_fiscalyear(self):
        self.start_period = None
        self.end_period = None


class PrintJournal(Wizard):
    'Print Journal'
    __name__ = 'account_jasper_reports.print_journal'
    start = StateView('account_jasper_reports.print_journal.start',
        'account_jasper_reports.print_journal_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateReport('account_jasper_reports.journal')

    def do_print_(self, action):
        start_period = self.start.fiscalyear.periods[0].id
        if self.start.start_period:
            start_period = self.start.start_period.id
        end_period = self.start.fiscalyear.periods[-1].id
        if self.start.end_period:
            end_period = self.start.end_period.id
        data = {
            'company': self.start.company.id,
            'open_close_account_moves': self.start.open_close_account_moves,
            'open_move_description': self.start.open_move_description,
            'close_move_description': self.start.close_move_description,
            'fiscalyear': self.start.fiscalyear.id,
            'start_period': start_period,
            'end_period': end_period,
            'journals': [x.id for x in self.start.journals],
            'output_format': self.start.output_format,
            }
        return action, data

    def transition_print_(self):
        return 'end'


class JournalReport(JasperReport):
    __name__ = 'account_jasper_reports.journal'

    @classmethod
    def _get_open_close_moves(cls, _type, description, fiscalyear, accounts,
            init_values, init_party_values, line):
        pool = Pool()
        Party = pool.get('party.party')
        Line = pool.get('account.move.line')
        Sequence = pool.get('ir.sequence')

        move = Line(line).move
        sequence = Sequence.search([
                ('id', '=', move.period.post_move_sequence_used.id),
                ])[0]
        sequence_prefix = Sequence._process(sequence.prefix, date=move.date)
        sequence_sufix = Sequence._process(sequence.suffix, date=move.date)
        number = move.post_number.replace(sequence_prefix, '').replace(
            sequence_sufix, '')

        if _type == 'open':
            number = int(0)
        else:
            number = int(number) + 1

        number = '%%0%sd' % sequence.padding % number
        move_post_number = '%s%s%s' % (
            sequence_prefix,
            number,
            sequence_sufix,
        )

        moves = []
        for account in accounts:
            account_type = 'other'
            if account.type.receivable:
                account_type = 'receivable'
            elif account.type.payable:
                account_type = 'payable'

            main_value = {}
            if _type == 'open':
                main_value['date'] = fiscalyear.start_date
                main_value['month'] = fiscalyear.start_date.month
                main_value['year'] = fiscalyear.start_date.year
                main_value['move_post_number'] = move_post_number
                main_value['move_number'] = move_post_number
                main_value['move_line_description'] = description
            else:
                main_value['date'] = fiscalyear.end_date
                main_value['month'] = fiscalyear.end_date.month
                main_value['year'] = fiscalyear.end_date.year
                main_value['move_post_number'] = move_post_number
                main_value['move_number'] = move_post_number
                main_value['move_line_description'] = description
            main_value['account_name'] = account.rec_name
            main_value['account_kind'] = account_type

            value = {}
            value.update(main_value)
            value['party_name'] = ''
            account_values = init_values.get(account.id, None)
            if account_values:
                balance = account_values.get('balance', 0)
                if balance:
                    if _type == 'open':
                        value['debit'] = (float(balance)
                            if balance >= 0 else 0)
                        value['credit'] = (-float(balance)
                            if balance < 0 else 0)
                    else:
                        value['debit'] = (-float(balance)
                            if balance < 0 else 0)
                        value['credit'] = (float(balance)
                            if balance >= 0 else 0)
                    moves.append(value)

            parties = init_party_values.get(account.id, None)
            if parties:
                for party_id, values in parties.items():
                    balance = values.get('balance', 0)
                    if balance:
                        value = {}
                        value.update(main_value)
                        if _type == 'open':
                            value['debit'] = (float(balance)
                                if balance >= 0 else 0)
                            value['credit'] = (-float(balance)
                                if balance < 0 else 0)
                        else:
                            value['debit'] = (-float(balance)
                                if balance < 0 else 0)
                            value['credit'] = (float(balance)
                                if balance >= 0 else 0)
                        value['party_name'] = Party(party_id).rec_name
                        moves.append(value)
        return moves

    @classmethod
    def prepare(cls, data):
        pool = Pool()
        Company = pool.get('company.company')
        FiscalYear = pool.get('account.fiscalyear')
        Account = pool.get('account.account')
        Party = pool.get('party.party')
        Journal = pool.get('account.journal')
        Period = pool.get('account.period')
        Line = pool.get('account.move.line')
        AccountType = pool.get('account.account.type')
        Move = pool.get('account.move')

        parameters = {}
        fiscalyear = FiscalYear(data['fiscalyear'])
        start_period = None
        if data['start_period']:
            start_period = Period(data['start_period'])
        end_period = None
        if data['end_period']:
            end_period = Period(data['end_period'])
        if data.get('open_close_account_moves'):
            journals = Journal.browse([])
        else:
            journals = Journal.browse(data.get('journals', []))

        company = None
        if data['company']:
            company = Company(data['company'])
        parameters['company'] = company.id
        parameters['company_rec_name'] = company and company.rec_name or ''
        parameters['company_vat'] = (company
            and company.party.tax_identifier
            and company.party.tax_identifier.code) or ''

        parameters['start_period'] = start_period and start_period.name or ''
        parameters['end_period'] = end_period and end_period.name or ''
        parameters['fiscal_year'] = fiscalyear.name
        if journals:
            parameters['journals'] = ','.join([x.name for x in journals])
        else:
            parameters['journals'] = ''

        if journals:
            journal_ids = ','.join([str(x.id) for x in journals])
            journals_domain = 'am.journal IN (%s) AND' % journal_ids
        else:
            journals_domain = ''

        periods = fiscalyear.get_periods(start_period, end_period)
        if periods:
            period_ids = ','.join([str(x.id) for x in periods])
            periods_domain = 'am.period IN (%s) AND' % period_ids
        else:
            periods_domain = ''

        cursor = Transaction().connection.cursor()
        cursor.execute("""
            SELECT
                aml.id
            FROM
                account_move am,
                account_move_line aml
            WHERE
                %s
                %s
                am.id=aml.move
            ORDER BY
                am.date, am.post_number, aml.id
        """ % (
                journals_domain,
                periods_domain,
                ))
        ids = [x[0] for x in cursor.fetchall()]

        open_moves = []
        close_moves = []
        # Preapre structure for the open/close move, if it's needed.
        if data.get('open_close_account_moves'):
            # First cehck if fiscal year before are closed
            fiscalyear_before_end_date = (
                fiscalyear.start_date - timedelta(days=1))
            fiscalyear_before = FiscalYear.search([
                ('end_date', '=', fiscalyear_before_end_date)])
            fiscalyear_before = (fiscalyear_before and fiscalyear_before[0]
                or None)

            with Transaction().set_context(active_test=False):
                accounts = Account.search([
                        ('parent', '!=', None),
                        ('type', '!=', Null),
                        ('active', 'in', [True, False]),
                        ])
                parties = Party.search([
                        ('active', 'in', [True, False]),
                        ])

            if fiscalyear_before and fiscalyear_before.state == 'close':
                # check if the start period is first period
                if (start_period.start_date
                        and (start_period.start_date - timedelta(days=1)
                            ) == fiscalyear_before.end_date):
                    initial_balance_date = (
                        start_period.start_date - timedelta(days=1))
                    with Transaction().set_context(date=initial_balance_date):
                        init_values = Account.read_account_vals(accounts,
                            with_moves=True, exclude_party_moves=True)
                        init_party_values = Party.get_account_values_by_party(
                            parties, accounts, fiscalyear.company)

                    open_moves.extend(cls._get_open_close_moves('open',
                        data.get('open_move_description'), fiscalyear,
                        accounts, init_values, init_party_values, ids[0]))

            if fiscalyear.state == 'close':
                # check if the last month is the same of the end month on
                #    fiscal year
                if (end_period.end_date
                        and end_period.end_date.month == fiscalyear.end_date.month):
                    with Transaction().set_context(date=end_period.end_date):
                        init_values = Account.read_account_vals(accounts,
                            with_moves=True, exclude_party_moves=True)
                        init_party_values = Party.get_account_values_by_party(
                            parties, accounts, fiscalyear.company)

                    close_moves.extend(cls._get_open_close_moves('close',
                        data.get('close_move_description'), fiscalyear,
                        accounts, init_values, init_party_values, ids[-1]))

        records = []
        records.extend(open_moves)

        move_line = Line.__table__()
        move = Move.__table__()
        party = Party.__table__()
        account = Account.__table__()
        account_type = AccountType.__table__()
        cursor.execute(*move_line.join(
            move,
            condition=move_line.move == move.id
        ).join(
            party, 'LEFT',
            condition=move_line.party == party.id
        ).join(
            account,
            condition=move_line.account == account.id
        ).join(
            account_type,
            condition=account.type == account_type.id
        ).select(
            move.date,
            Extract('MONTH', move.date).cast('int').as_('month'),
            Extract('YEAR', move.date).cast('int').as_('year'),
            Case((account.code != Null,
                Concat(Concat(account.code, ' - '), account.name)),
                else_=account.name).as_('account_name'),
            move.number.as_('move_number'),
            move.post_number.as_('move_post_number'),
            move_line.description.as_('move_line_description'),
            move_line.debit,
            move_line.credit,
            Coalesce(party.name, '').as_('party_name'),
            Case((account_type.receivable, 'receivable'),
                else_=Case((account_type.payable, 'payable'),
                    else_='other')).as_('account_kind'),
            where=reduce_ids(move_line.id, ids)
        ))
        row_fields = ['date', 'month', 'year', 'account_name', 'move_number',
            'move_post_number', 'move_line_description', 'debit', 'credit',
            'party_name', 'account_kind']
        for row in cursor.fetchall():
            records.append(dict(zip(row_fields, row)))

        records.extend(close_moves)
        return records, parameters

    @classmethod
    def execute(cls, ids, data):
        records, parameters = cls.prepare(data)
        return super(JournalReport, cls).execute(ids, {
                'name': 'account_jasper_reports.journal',
                'model': 'account.move.line',
                'data_source': 'records',
                'records': records,
                'parameters': parameters,
                'output_format': data['output_format'],
                })

    @classmethod
    def render(cls, action_report, data, model, ids):
        if data.get('output_format') == 'csv':
            return cls.render_csv(action_report, data)
        return super().render(action_report, data, model, ids)

    @classmethod
    def render_csv(cls, report, data):
        pool = Pool()
        Lang = pool.get('ir.lang')
        Company = pool.get('company.company')
        lang, = Lang.search([
            'code', '=', (Transaction().context.get('language'))])
        output = BytesIO()
        header = ['Move post number', 'Date', 'Account name',
            'Move line description', 'Debit', 'Credit']
        header_translated = []
        for head in header:
            header_translated.append(cls._get_report_translation(head, lang))
        writer = csv.writer(
            TextIOWrapper(output, encoding='utf-8', write_through=True),
            delimiter=';')
        writer.writerow(header_translated)
        total_amount = [0, 0]

        def get_grouping_key(record):
            return record.get('year'), record.get('month')

        company = Company(data['parameters']['company'])
        currency = company.currency
        for month, rows in groupby(sorted(data.get('records'),
                key=lambda x: get_grouping_key(x)),
                key=lambda x: get_grouping_key(x)):

            year, month = month
            rows = sorted(rows,
                key=lambda r: (r['date'], r['move_post_number'] or ''))
            head_month = '%s %s' % (
                cls._get_report_translation('Total month', lang), month)
            total_month = ['', '', '', head_month, 0, 0]
            for row in rows:
                writer.writerow([
                    row.get('move_post_number'),
                    cls.format_date(row.get('date'), lang),
                    row.get('account_name')
                    if not row.get('party_name')
                    else '%s / %s' % (
                        row.get('account_name').split('-')[0].rstrip(),
                        row['party_name']),
                    row.get('move_line_description'),
                    cls.format_number(Decimal(row.get('debit')), lang),
                    cls.format_number(Decimal(row.get('credit')), lang),
                ])
                total_month[4] += Decimal(row.get('debit'))
                total_month[5] += Decimal(row.get('credit'))
            if currency:
                total_month[4] = currency.round(total_month[4])
                total_month[5] = currency.round(total_month[5])

            total_amount[0] += total_month[4]
            total_amount[1] += total_month[5]
            total_month[4] = cls.format_number(total_month[4], lang)
            total_month[5] = cls.format_number(total_month[5], lang)
            writer.writerow(total_month)

        total_debit, total_credit = total_amount
        if currency:
            total_debit = currency.round(total_debit)
            total_credit = currency.round(total_credit)
        writer.writerow([
                '', '', '',
                cls._get_report_translation('Total', lang),
                cls.format_number(total_debit, lang),
                cls.format_number(total_credit, lang),
            ])
        return (data['output_format'], output.getvalue(), '')

    @classmethod
    def _get_report_translation(cls, string, lang):
        pool = Pool()
        Translation = pool.get('ir.translation')

        return Translation.get_source(cls.__name__, 'report', lang.code,
            string) or string
